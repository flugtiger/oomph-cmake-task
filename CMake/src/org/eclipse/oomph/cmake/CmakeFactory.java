/**
 */
package org.eclipse.oomph.cmake;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.oomph.cmake.CmakePackage
 * @generated
 */
public interface CmakeFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  CmakeFactory eINSTANCE = org.eclipse.oomph.cmake.impl.CmakeFactoryImpl.init();

  /**
   * Returns a new object of class '<em>CMake Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>CMake Task</em>'.
   * @generated
   */
  CMakeTask createCMakeTask();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  CmakePackage getCmakePackage();

} // CmakeFactory
