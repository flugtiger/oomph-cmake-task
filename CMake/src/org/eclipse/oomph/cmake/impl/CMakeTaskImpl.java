/*
 * Copyright (c) 2014 Eike Stepper (Berlin, Germany) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package org.eclipse.oomph.cmake.impl;

import org.eclipse.oomph.cmake.CMakeTask;
import org.eclipse.oomph.cmake.CmakePackage;
import org.eclipse.oomph.setup.SetupTaskContext;
import org.eclipse.oomph.setup.impl.SetupTaskImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CMake Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CMakeTaskImpl extends SetupTaskImpl implements CMakeTask
{
  private static final int PRIORITY = PRIORITY_DEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CMakeTaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CmakePackage.Literals.CMAKE_TASK;
  }

  @Override
  public int getPriority()
  {
    return PRIORITY;
  }

  public boolean isNeeded(SetupTaskContext context) throws Exception
  {
    // TODO Implement CMakeTaskImpl.isNeeded()
    return true;
  }

  public void perform(SetupTaskContext context) throws Exception
  {
    // TODO Implement CMakeTaskImpl.perform()
    context.log("Hallo Welt!");
  }

  @Override
  public void dispose()
  {
    // TODO Implement CMakeTaskImpl.perform() or remove this override if not needed
  }

} // CMakeTaskImpl
