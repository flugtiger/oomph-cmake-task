/**
 */
package org.eclipse.oomph.cmake.impl;

import org.eclipse.oomph.cmake.CMakeTask;
import org.eclipse.oomph.cmake.CmakeFactory;
import org.eclipse.oomph.cmake.CmakePackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CmakeFactoryImpl extends EFactoryImpl implements CmakeFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static CmakeFactory init()
  {
    try
    {
      CmakeFactory theCmakeFactory = (CmakeFactory)EPackage.Registry.INSTANCE.getEFactory(CmakePackage.eNS_URI);
      if (theCmakeFactory != null)
      {
        return theCmakeFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new CmakeFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmakeFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
    case CmakePackage.CMAKE_TASK:
      return createCMakeTask();
    default:
      throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CMakeTask createCMakeTask()
  {
    CMakeTaskImpl cMakeTask = new CMakeTaskImpl();
    return cMakeTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CmakePackage getCmakePackage()
  {
    return (CmakePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static CmakePackage getPackage()
  {
    return CmakePackage.eINSTANCE;
  }

} // CmakeFactoryImpl
