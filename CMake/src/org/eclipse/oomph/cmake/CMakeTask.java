/*
 * Copyright (c) 2014 Eike Stepper (Berlin, Germany) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eike Stepper - initial API and implementation
 */
package org.eclipse.oomph.cmake;

import org.eclipse.oomph.setup.SetupTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CMake Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.oomph.cmake.CmakePackage#getCMakeTask()
 * @model annotation="http://www.eclipse.org/oomph/setup/Enablement variableName='p2.cmake' repository='https://gitlab.com/flugtiger/eclipse-update-site/raw/master' installableUnits='CMake.feature.group'"
 *        annotation="http://www.eclipse.org/oomph/setup/ValidTriggers triggers='STARTUP MANUAL'"
 * @generated
 */
public interface CMakeTask extends SetupTask
{

} // CMakeTask
